import React from 'react';
import { StyleSheet, Image, View, Dimensions, Text } from 'react-native';
import UnityView, {UnityModule} from 'react-native-unity-view';

export default class App extends React.Component{
    render() {
    return (
      <View>
        <UnityView style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, }}></UnityView>
        <Text>
          Welcome to React Native!
        </Text>
      </View>
    );
  }
}

/*
import UnityView, {UnityModule} from 'react-native-unity-view';
React.createElement(UnityView, {
style: {position: 'absolute', left: 0, right: 0, top: 0, bottom: 0},
onUnityMessage: this.onUnityMessage.bind(this),
});
*/