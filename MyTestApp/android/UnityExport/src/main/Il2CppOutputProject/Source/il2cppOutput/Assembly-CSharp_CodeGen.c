﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 <id>j__TPar <>f__AnonymousType0`4::get_id()
// 0x00000002 <seq>j__TPar <>f__AnonymousType0`4::get_seq()
// 0x00000003 <name>j__TPar <>f__AnonymousType0`4::get_name()
// 0x00000004 <data>j__TPar <>f__AnonymousType0`4::get_data()
// 0x00000005 System.Void <>f__AnonymousType0`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x00000006 System.Boolean <>f__AnonymousType0`4::Equals(System.Object)
// 0x00000007 System.Int32 <>f__AnonymousType0`4::GetHashCode()
// 0x00000008 System.String <>f__AnonymousType0`4::ToString()
// 0x00000009 System.Void DestroyAfterDelay::Start()
extern void DestroyAfterDelay_Start_m627A0FBA948E4C0C248E8D36684D8107C7866AFF (void);
// 0x0000000A System.Void DestroyAfterDelay::Update()
extern void DestroyAfterDelay_Update_mF9FF3B092DF3EFB318C4CBC4ED99EFA7445CCA5A (void);
// 0x0000000B System.Void DestroyAfterDelay::.ctor()
extern void DestroyAfterDelay__ctor_m85BF2667EB62D2D93F433169ADC9939F37A12E2A (void);
// 0x0000000C System.Void DestroyOutOfBounds::Start()
extern void DestroyOutOfBounds_Start_mF1982FE2F4AF5912172A4359E1C65ED580ABD67C (void);
// 0x0000000D System.Void DestroyOutOfBounds::Update()
extern void DestroyOutOfBounds_Update_m2337730029C731551291E932F658C0E098AAFE59 (void);
// 0x0000000E System.Void DestroyOutOfBounds::.ctor()
extern void DestroyOutOfBounds__ctor_mB18D293D861B36C889E2E31B07673E10C0CDD5DC (void);
// 0x0000000F System.Void DetectCollisions::Start()
extern void DetectCollisions_Start_mAE7A4B06ADF423C95E1DA751AD71E0D047552B4F (void);
// 0x00000010 System.Void DetectCollisions::Update()
extern void DetectCollisions_Update_mCCAE269E4C3B9BD943E227B49AED8F542F3A7B5C (void);
// 0x00000011 System.Void DetectCollisions::OnTriggerEnter(UnityEngine.Collider)
extern void DetectCollisions_OnTriggerEnter_m4EEFDB371DEBDBF5A256720A3B8F1F4B35112704 (void);
// 0x00000012 System.Void DetectCollisions::.ctor()
extern void DetectCollisions__ctor_mE4563DA16E143370A82855DDDF2C4C84B265B473 (void);
// 0x00000013 System.Void DifficultyButton::Start()
extern void DifficultyButton_Start_m1F261BAF9C46DDCC1C6CDF4AC6DE2CB3C1C7C309 (void);
// 0x00000014 System.Void DifficultyButton::Update()
extern void DifficultyButton_Update_m30DBD52ED7D09F429517637A9E0719D1E790F7D6 (void);
// 0x00000015 System.Void DifficultyButton::SetDifficulty()
extern void DifficultyButton_SetDifficulty_m22E086216CC14422C59CEABB6335D74333170074 (void);
// 0x00000016 System.Void DifficultyButton::.ctor()
extern void DifficultyButton__ctor_m6819E6CE54D449BB0A8365215DE721937F21BFA8 (void);
// 0x00000017 System.Void MoveForward::Start()
extern void MoveForward_Start_m01A14393269317C9EC0DE9C8B34C219475B5B9D0 (void);
// 0x00000018 System.Void MoveForward::Update()
extern void MoveForward_Update_m1D1EA1D175B6E5FED262E5D66BD0F05A5A6C1B1E (void);
// 0x00000019 System.Void MoveForward::.ctor()
extern void MoveForward__ctor_m79E20E057C5117B7FC19B2CB3146FCAF61ABE123 (void);
// 0x0000001A System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x0000001B System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x0000001C System.Void PlayerController::SpawnPizza()
extern void PlayerController_SpawnPizza_m0331F5353271B2D5617CB496452286A9C2225404 (void);
// 0x0000001D System.Void PlayerController::RunCharacter(System.Single)
extern void PlayerController_RunCharacter_m8CCCB92BE07A488C8FC67CF5D921B8B5A4E06132 (void);
// 0x0000001E System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x0000001F System.Void SpawnManager::Start()
extern void SpawnManager_Start_mFA050B5C59C7CC47F4430CA0F5A646051D5442CB (void);
// 0x00000020 System.Void SpawnManager::Update()
extern void SpawnManager_Update_m68C1739F474F0D29A0BF533BD44BA82786A3C96B (void);
// 0x00000021 System.Void SpawnManager::StartGame(System.Int32)
extern void SpawnManager_StartGame_mB6F2DE50514225E4AA0CDF7099CB8F1A0FBEE20B (void);
// 0x00000022 System.Void SpawnManager::SpawnRandomAnimal()
extern void SpawnManager_SpawnRandomAnimal_m6FEC50772F07738073875335C14F1E6BD2414FD0 (void);
// 0x00000023 System.Void SpawnManager::UpdateScore(System.Int32)
extern void SpawnManager_UpdateScore_mB3FC9A47306DFD62565FC7983ABB7C0F5A461794 (void);
// 0x00000024 System.Void SpawnManager::GameOver()
extern void SpawnManager_GameOver_m6F3524D81F0E3BF1DB82B5E52CC6B9D716BC9208 (void);
// 0x00000025 System.Void SpawnManager::RestartGame()
extern void SpawnManager_RestartGame_mE166F7E57290F7D3E138A5C3A1AE7B1D811DB4C6 (void);
// 0x00000026 System.Void SpawnManager::InfoFunc(System.String)
extern void SpawnManager_InfoFunc_mA570271D1D06ADB1A4621692E813323D252DCA3B (void);
// 0x00000027 System.Void SpawnManager::.ctor()
extern void SpawnManager__ctor_mBCD48EEAB1EB733A88D47C85ACC373C796F20E59 (void);
// 0x00000028 MessageHandler MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mFD7001A5166EDD8386B2979B3C0FDC4544D0F123 (void);
// 0x00000029 T MessageHandler::getData()
// 0x0000002A System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_m88684D58D64080CE7766BEE3D2D32EA5193A00B4 (void);
// 0x0000002B System.Void MessageHandler::send(System.Object)
extern void MessageHandler_send_m9EF609A41518571430690AFA490250C054CA5C4B (void);
// 0x0000002C System.Void UnityMessage::.ctor()
extern void UnityMessage__ctor_m31453D518E0E54753A5581B23A7ABEFD6165C537 (void);
// 0x0000002D System.Int32 UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m044493EEA44FD0FE6040EAAC4F26AF0AA65D32B1 (void);
// 0x0000002E UnityMessageManager UnityMessageManager::get_Instance()
extern void UnityMessageManager_get_Instance_mB4DBA7125B044AAAD8F2ABE7B14FCC2A96C2E762 (void);
// 0x0000002F System.Void UnityMessageManager::set_Instance(UnityMessageManager)
extern void UnityMessageManager_set_Instance_m75E2A324F9962814F06D1448B749BB938EE10009 (void);
// 0x00000030 System.Void UnityMessageManager::add_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m7F72EFB7A6934E92EE91211BE100AAFAC60F1EF8 (void);
// 0x00000031 System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager/MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_mFBE226643D6E84B5D7931C507A60D9C965B8E2B0 (void);
// 0x00000032 System.Void UnityMessageManager::add_OnRNMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_add_OnRNMessage_mC593FA3E1413EDF828FBCFE38A62A1C8876FA9F5 (void);
// 0x00000033 System.Void UnityMessageManager::remove_OnRNMessage(UnityMessageManager/MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnRNMessage_mEFF0D5E32F91D81EF4B5B14CFFE7B37DFD43AA7E (void);
// 0x00000034 System.Void UnityMessageManager::.cctor()
extern void UnityMessageManager__cctor_m89611F0C2BAAF282BE647049B11F835262EFDB73 (void);
// 0x00000035 System.Void UnityMessageManager::Awake()
extern void UnityMessageManager_Awake_m5F7D76ABE1A8F8F9786EB0D85D90E9028B51007E (void);
// 0x00000036 System.Void UnityMessageManager::SendMessageToRN(System.String)
extern void UnityMessageManager_SendMessageToRN_m1D4A6F48DA84FD379FCE5F46FDAFDC7772E52F70 (void);
// 0x00000037 System.Void UnityMessageManager::SendMessageToRN(UnityMessage)
extern void UnityMessageManager_SendMessageToRN_mE6757FAB6C5E7F5C303A482693E463729BB28C60 (void);
// 0x00000038 System.Void UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_m4D0F43F8D328F97496576D9930AAB3686E6A8558 (void);
// 0x00000039 System.Void UnityMessageManager::onRNMessage(System.String)
extern void UnityMessageManager_onRNMessage_m911408001E45CF36B3E11A0E7BABF053AE6166AA (void);
// 0x0000003A System.Void UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_m03CBB4E08C8BA079E07D430AD377D38400DB82BF (void);
// 0x0000003B System.Void UnityMessageManager/MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_mABBBB0720CEC9ADB6560700A2EE640194D040EF8 (void);
// 0x0000003C System.Void UnityMessageManager/MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m38A45D4D87F60621AB4E4B6A68DDDE146B332DDD (void);
// 0x0000003D System.IAsyncResult UnityMessageManager/MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_m5FD705274B59AF179AE22FF30A248A4A24B4BECA (void);
// 0x0000003E System.Void UnityMessageManager/MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_m8EB1088C4795F088A729524E99334E63ED48E6B7 (void);
// 0x0000003F System.Void UnityMessageManager/MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_m27BFC33CA6935FF169ED735C704019CD4DF0E8C1 (void);
// 0x00000040 System.Void UnityMessageManager/MessageHandlerDelegate::Invoke(MessageHandler)
extern void MessageHandlerDelegate_Invoke_m001CCB602D2B55DD220BF06E2AA4828575197FFE (void);
// 0x00000041 System.IAsyncResult UnityMessageManager/MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_m6F3C7C037B912D8BD1CB2FAB6CF0E8D8CB3825BA (void);
// 0x00000042 System.Void UnityMessageManager/MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_mE67B6A56FC2D25FC6BAE72DF3DDA9A01260DF3D2 (void);
static Il2CppMethodPointer s_methodPointers[66] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DestroyAfterDelay_Start_m627A0FBA948E4C0C248E8D36684D8107C7866AFF,
	DestroyAfterDelay_Update_mF9FF3B092DF3EFB318C4CBC4ED99EFA7445CCA5A,
	DestroyAfterDelay__ctor_m85BF2667EB62D2D93F433169ADC9939F37A12E2A,
	DestroyOutOfBounds_Start_mF1982FE2F4AF5912172A4359E1C65ED580ABD67C,
	DestroyOutOfBounds_Update_m2337730029C731551291E932F658C0E098AAFE59,
	DestroyOutOfBounds__ctor_mB18D293D861B36C889E2E31B07673E10C0CDD5DC,
	DetectCollisions_Start_mAE7A4B06ADF423C95E1DA751AD71E0D047552B4F,
	DetectCollisions_Update_mCCAE269E4C3B9BD943E227B49AED8F542F3A7B5C,
	DetectCollisions_OnTriggerEnter_m4EEFDB371DEBDBF5A256720A3B8F1F4B35112704,
	DetectCollisions__ctor_mE4563DA16E143370A82855DDDF2C4C84B265B473,
	DifficultyButton_Start_m1F261BAF9C46DDCC1C6CDF4AC6DE2CB3C1C7C309,
	DifficultyButton_Update_m30DBD52ED7D09F429517637A9E0719D1E790F7D6,
	DifficultyButton_SetDifficulty_m22E086216CC14422C59CEABB6335D74333170074,
	DifficultyButton__ctor_m6819E6CE54D449BB0A8365215DE721937F21BFA8,
	MoveForward_Start_m01A14393269317C9EC0DE9C8B34C219475B5B9D0,
	MoveForward_Update_m1D1EA1D175B6E5FED262E5D66BD0F05A5A6C1B1E,
	MoveForward__ctor_m79E20E057C5117B7FC19B2CB3146FCAF61ABE123,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_SpawnPizza_m0331F5353271B2D5617CB496452286A9C2225404,
	PlayerController_RunCharacter_m8CCCB92BE07A488C8FC67CF5D921B8B5A4E06132,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	SpawnManager_Start_mFA050B5C59C7CC47F4430CA0F5A646051D5442CB,
	SpawnManager_Update_m68C1739F474F0D29A0BF533BD44BA82786A3C96B,
	SpawnManager_StartGame_mB6F2DE50514225E4AA0CDF7099CB8F1A0FBEE20B,
	SpawnManager_SpawnRandomAnimal_m6FEC50772F07738073875335C14F1E6BD2414FD0,
	SpawnManager_UpdateScore_mB3FC9A47306DFD62565FC7983ABB7C0F5A461794,
	SpawnManager_GameOver_m6F3524D81F0E3BF1DB82B5E52CC6B9D716BC9208,
	SpawnManager_RestartGame_mE166F7E57290F7D3E138A5C3A1AE7B1D811DB4C6,
	SpawnManager_InfoFunc_mA570271D1D06ADB1A4621692E813323D252DCA3B,
	SpawnManager__ctor_mBCD48EEAB1EB733A88D47C85ACC373C796F20E59,
	MessageHandler_Deserialize_mFD7001A5166EDD8386B2979B3C0FDC4544D0F123,
	NULL,
	MessageHandler__ctor_m88684D58D64080CE7766BEE3D2D32EA5193A00B4,
	MessageHandler_send_m9EF609A41518571430690AFA490250C054CA5C4B,
	UnityMessage__ctor_m31453D518E0E54753A5581B23A7ABEFD6165C537,
	UnityMessageManager_generateId_m044493EEA44FD0FE6040EAAC4F26AF0AA65D32B1,
	UnityMessageManager_get_Instance_mB4DBA7125B044AAAD8F2ABE7B14FCC2A96C2E762,
	UnityMessageManager_set_Instance_m75E2A324F9962814F06D1448B749BB938EE10009,
	UnityMessageManager_add_OnMessage_m7F72EFB7A6934E92EE91211BE100AAFAC60F1EF8,
	UnityMessageManager_remove_OnMessage_mFBE226643D6E84B5D7931C507A60D9C965B8E2B0,
	UnityMessageManager_add_OnRNMessage_mC593FA3E1413EDF828FBCFE38A62A1C8876FA9F5,
	UnityMessageManager_remove_OnRNMessage_mEFF0D5E32F91D81EF4B5B14CFFE7B37DFD43AA7E,
	UnityMessageManager__cctor_m89611F0C2BAAF282BE647049B11F835262EFDB73,
	UnityMessageManager_Awake_m5F7D76ABE1A8F8F9786EB0D85D90E9028B51007E,
	UnityMessageManager_SendMessageToRN_m1D4A6F48DA84FD379FCE5F46FDAFDC7772E52F70,
	UnityMessageManager_SendMessageToRN_mE6757FAB6C5E7F5C303A482693E463729BB28C60,
	UnityMessageManager_onMessage_m4D0F43F8D328F97496576D9930AAB3686E6A8558,
	UnityMessageManager_onRNMessage_m911408001E45CF36B3E11A0E7BABF053AE6166AA,
	UnityMessageManager__ctor_m03CBB4E08C8BA079E07D430AD377D38400DB82BF,
	MessageDelegate__ctor_mABBBB0720CEC9ADB6560700A2EE640194D040EF8,
	MessageDelegate_Invoke_m38A45D4D87F60621AB4E4B6A68DDDE146B332DDD,
	MessageDelegate_BeginInvoke_m5FD705274B59AF179AE22FF30A248A4A24B4BECA,
	MessageDelegate_EndInvoke_m8EB1088C4795F088A729524E99334E63ED48E6B7,
	MessageHandlerDelegate__ctor_m27BFC33CA6935FF169ED735C704019CD4DF0E8C1,
	MessageHandlerDelegate_Invoke_m001CCB602D2B55DD220BF06E2AA4828575197FFE,
	MessageHandlerDelegate_BeginInvoke_m6F3C7C037B912D8BD1CB2FAB6CF0E8D8CB3825BA,
	MessageHandlerDelegate_EndInvoke_mE67B6A56FC2D25FC6BAE72DF3DDA9A01260DF3D2,
};
static const int32_t s_InvokerIndices[66] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2646,
	2646,
	2646,
	2646,
	2646,
	2646,
	2646,
	2646,
	2194,
	2646,
	2646,
	2646,
	2646,
	2646,
	2646,
	2646,
	2646,
	2646,
	2646,
	2646,
	2216,
	2646,
	2646,
	2646,
	2181,
	2646,
	2181,
	2646,
	2646,
	2194,
	2646,
	3890,
	-1,
	538,
	2194,
	2646,
	4058,
	4063,
	4027,
	2194,
	2194,
	2194,
	2194,
	4079,
	2646,
	2194,
	2194,
	2194,
	2194,
	2646,
	1297,
	2194,
	701,
	2194,
	1297,
	2194,
	701,
	2194,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000002, { 0, 21 } },
	{ 0x06000029, { 21, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[22] = 
{
	{ (Il2CppRGCTXDataType)2, 1180 },
	{ (Il2CppRGCTXDataType)3, 5646 },
	{ (Il2CppRGCTXDataType)2, 1756 },
	{ (Il2CppRGCTXDataType)3, 5644 },
	{ (Il2CppRGCTXDataType)3, 5696 },
	{ (Il2CppRGCTXDataType)2, 1778 },
	{ (Il2CppRGCTXDataType)3, 5694 },
	{ (Il2CppRGCTXDataType)3, 5711 },
	{ (Il2CppRGCTXDataType)2, 1785 },
	{ (Il2CppRGCTXDataType)3, 5709 },
	{ (Il2CppRGCTXDataType)3, 5720 },
	{ (Il2CppRGCTXDataType)2, 1789 },
	{ (Il2CppRGCTXDataType)3, 5718 },
	{ (Il2CppRGCTXDataType)3, 5645 },
	{ (Il2CppRGCTXDataType)3, 5695 },
	{ (Il2CppRGCTXDataType)3, 5710 },
	{ (Il2CppRGCTXDataType)3, 5719 },
	{ (Il2CppRGCTXDataType)2, 358 },
	{ (Il2CppRGCTXDataType)2, 728 },
	{ (Il2CppRGCTXDataType)2, 864 },
	{ (Il2CppRGCTXDataType)2, 920 },
	{ (Il2CppRGCTXDataType)3, 17816 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	66,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	22,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
